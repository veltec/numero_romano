package dojo3;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class NumeroRomanoTest {

    @Test
    public void para_I_deveRetornar_1() {
        String entrada = "I";

        int retorno = NumeroRomano.converter(entrada);

        assertEquals(1, retorno);
    }

    @Test
    public void para_II_deveRetornar_2() {
        String entrada = "II";

        int retorno = NumeroRomano.converter(entrada);

        assertEquals(2, retorno);
    }


    @Test
    public void para_IV_deveRetornar_4() {
        String entrada = "IV";

        int retorno = NumeroRomano.converter(entrada);

        assertEquals(4, retorno);
    }

    @Test
    public void para_XIV_deveRetornar_14() {
        String entrada = "XIV";

        int retorno = NumeroRomano.converter(entrada);

        assertEquals(14, retorno);
    }

    @Test
    public void para_V_deveRetornar_5() {
        String entrada = "V";

        int retorno = NumeroRomano.converter(entrada);

        assertEquals(5, retorno);
    }

    @Test
    public void para_X_deveRetornar_10() {
        String entrada = "X";

        int retorno = NumeroRomano.converter(entrada);

        assertEquals(10, retorno);
    }

    @Test
    public void para_MMM_deveRetornar_3000() {
        String entrada = "MMM";

        int retorno = NumeroRomano.converter(entrada);

        assertEquals(3000, retorno);
    }


    static class NumeroRomano {

        public static int converter(String entrada) {

            // IV
            // 0 1 | 1 5
            int valor = 0;
            int valorAtual = 0;
            Integer tamanhoEntrada = entrada.length();
            Integer valorAnterior = 0;
            for (int i = 0; i < tamanhoEntrada; i++){
                valorAtual = EnumRomano.valueOf(""+entrada.charAt(i)).valor;

                if(valorAtual > valorAnterior) {
                    valor -= valorAnterior;
                } else {
                    valor += valorAtual;
                }

                valorAnterior = EnumRomano.valueOf(""+entrada.charAt(i)).valor;
            }
            valor += valorAtual;
//            char primeiroDigito = entrada.charAt(0);
//            char segundoDigito = entrada.charAt(1);

            return valor;
        }
    }

}
